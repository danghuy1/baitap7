class Movie {
  final int id;
  final String title;
  final String posterPath;
  final String overview;

  Movie(
      {required this.title,
      required this.posterPath,
      required this.overview,
      required this.id});

  factory Movie.fromMap(Map<String, dynamic> map) {
    return Movie(
        id: map['id'],
        title: map['title'],
        posterPath: map['poster_path'],
        overview: map['overview']);
  }
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'poster_path': posterPath,
      'overview': overview
    };
  }
}
