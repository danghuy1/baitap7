import 'package:dio/dio.dart';
import 'package:untitled1/model/movie_model.dart';

const apiKey = '70f275559b9ce95232198e3d84a32320';

class Api {
  final popularApiUrl =
      'https://api.themoviedb.org/3/movie/popular?api_key=$apiKey';

  Future<List<Movie>> getPopularMovies() async {
    final dio = Dio();
    final response = await dio.get(
      popularApiUrl,
      queryParameters: {'api_key': apiKey, 'language': 'vi-VN', 'page': 1},
    );

    if (response.statusCode == 200) {
      final results = response.data['results'] as List<dynamic>;
      return results.map((movieData) => Movie.fromMap(movieData)).toList();
    } else {
      throw Exception(
          'Failed to load movie popular. Status code: ${response.statusCode}');
    }
  }

  Future<Movie> getMovieDetails(int movieId) async {
    final dio = Dio();
    final response = await dio.get(
      'https://api.themoviedb.org/3/movie/$movieId',
      queryParameters: {'api_key': apiKey, 'language': 'vi-VN'},
    );

    if (response.statusCode == 200) {
      final movieData = response.data;
      return Movie.fromMap(movieData);
    } else {
      throw Exception(
          'Failed to load movie details. Status code: ${response.statusCode}');
    }
  }
}
